#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

cd /tmp
mkdir cf
mkdir cf/root
cp /etc/cloudflared/* cf
cp /root/.cloudflared/* cf/root
tar czvf cf.tgz cf/
cd -
mv  /tmp/cf.tgz .
echo "Config now in ./cf.tgz"
