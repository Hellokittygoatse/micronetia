#!/bin/bash -e
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi
FQDN='invalid'

echo "Current Cloudflared version"
cloudflared --version

read -p "Do you want to try to download a newer version of cloudflared?" ANSWER
if [ "$ANSWER" != "${ANSWER#[Yy]}" ] ;then
  echo "Downloading the latest cloudflared"

  curl -o /tmp/cloudflared-stable-linux-arm.tgz https://bin.equinox.io/c/VdrWdbjqyF/cloudflared-stable-linux-arm.tgz
  cd /usr/local/bin
  tar xzvf /tmp/cloudflared-stable-linux-arm.tgz
fi

echo "Connecting to cloudflare to log this device into your account, copy the URL into your browser"

cloudflared tunnel login

echo "Creating Cloudflared tunnel, routing it to this pi and so on."
echo 
while [ true ] ; do
  read -p "Blog hostname (e.g. blog.mydomain.com): " FQDN
  echo "Blog to be hosted at https://$FQDN/"
  
  read -p "Is this correct? (Y/N): " ANSWER
  if [ "$ANSWER" != "${ANSWER#[Yy]}" ] ;then
#    echo "Validating name"
#    if [ `echo $FQDN|sed 's/[^a-zA-Z0-9\.\-]//g' | wc -m` != `echo $FQDN | wc -m` ] ; then echo "Got bad chars"; fi
#    if [ `echo $FQDN|sed 's/\..*//' | wc -m` != `echo $FQDN | wc -m` ] ; then echo "Got dots"; fi
#    if [ -z `dig +short miconetia.i4tru.st | head -n 1` ] ;then echo "CNAME can be created"; fi
    break
  else
    echo "please reenter correct values"
  fi
done
TUNNEL=`echo $FQDN|sed 's/\..*//'`
if [ "$FQDN" == "$TUNNEL" ] ; then
  echo "$FQDN does not seem to be a valid domain name."
  exit 1;
fi

cloudflared tunnel create "${TUNNEL}-tunnel"
CRED=`cloudflared tunnel list | sed -n "/-tunnel/ s/ $TUNNEL.*//p"`

cloudflared tunnel route dns "${TUNNEL}-tunnel" $FQDN


echo "hostname: $FQDN" >/root/.cloudflared/config.yml
echo "hello-world: true" >>/root/.cloudflared/config.yml

cloudflared tunnel run "${TUNNEL}-tunnel"

sed 's!hello.*!url: http://localhost:2368!' -i /root/.cloudflared/config.yml 
echo "tunnel: $CRED" >>/root/.cloudflared/config.yml
echo "credentials-file: /root/.cloudflared/$CRED.json" >>/root/.cloudflared/config.yml

echo -e "Check to make sure no errors\n\nconfig.yml"
cat /root/.cloudflared/config.yml
echo "\n" $CRED.json
cat /root/.cloudflared/$CRED.json

read -p "Install as service?" ANSWER
if [ "$ANSWER" != "${ANSWER#[Yy]}" ] ;then
  cloudflared service install
  service cloudflared start
  echo "updating the ghost config to use https://$FQDN/"
  sed 's!http://[^"]*!https://'"$FQDN/!" -i /home/server/blog/config.development.json
  cd /home/server/blog/
  sudo -u server ghost restart
  sed '
/exit 0/ i\
cd /home/server/blog; sudo -u server ghost start
' -i /etc/rc.local

else 
  echo "service not installed. You'll need to do\nsudo cloudflared service install"
  echo "sudo service cloudflared start\nto have the service installed."
fi

