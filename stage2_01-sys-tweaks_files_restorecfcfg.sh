#!/bin/bash

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

TAR=${1:-'./cf.tgz'}
if [ ! -e $TAR ] ; then
  echo "$TAR not found cannot restore"
  exit 1
fi
tar xzvf $TAR -C /tmp
cd /tmp

if [ ! -e cf ] || [ ! -e cf/root ] ; then
  echo "$TAR did appear to contain expected folders, cannot restore"
  exit 1
fi

if [ ! -e /etc/cloudflared ]; then
  echo "no /etc/cloudflared directory, creating"
  mkdir /etc/cloudflared
fi
cp cf/* /etc/cloudflared
if [ ! -e /root/.cloudflared/ ]; then
  echo "no /root/.cloudflared/ directory, creating"
  mkdir /root/.cloudflared/
fi
cp cf/root/* /root/.cloudflared/

echo "config restored"
