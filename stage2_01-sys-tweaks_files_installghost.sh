#!/bin/bash -e
if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  exit 1
fi

curl -sL https://deb.nodesource.com/setup_12.x | bash -
apt install nodejs
npm install ghost-cli@latest -g
chmod a+w /home/server/blog
chown server:server /home/server/blog

echo -e "----------------------------------\n Node.js and ghost cli installed"
echo -e "\nNow installing ghost itself into ~/blog\n\n"
cd /home/server/blog
sudo -u server ghost install local

echo "checking ghost worked"
curl http://localhost:2368/ 

read -p "Did you get a load of HTML? (Y/N): " ANSWER
if [ "$ANSWER" != "${ANSWER#[Yy]}" ] ;then
  H=`hostname -I| sed 's/ //g'`
  sed "s/MICRONETIA/$H/" /home/server/templates/ghost80 | sudo tee /etc/nginx/sites-enabled/ghost80 
  service nginx reload
  echo "Now go to http://$H/ghost/ in your browser to create a ghost user etc." 
fi
